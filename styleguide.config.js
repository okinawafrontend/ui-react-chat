const path = require('path')

module.exports = {
  title: `Atomic Design`,
  showUsage: true,
  getComponentPathLine: (componentPath) => {
    const name = path.basename(componentPath, '.jsx')
    const dir = path.dirname(componentPath)
    return `import ${name} from '${dir}';`
  },
  sections: [
    {
      name: 'atoms',
      components: () => ([
        path.resolve(__dirname, 'src/components/atoms/Background', 'Background.jsx'),
        path.resolve(__dirname, 'src/components/atoms/Btn', 'Btn.jsx'),
        path.resolve(__dirname, 'src/components/atoms/BtnIcon', 'BtnIcon.jsx'),
        path.resolve(__dirname, 'src/components/atoms/BtnLg', 'BtnLg.jsx'),
        path.resolve(__dirname, 'src/components/atoms/Input', 'Input.jsx'),
      ])
    },{
      name: 'molecules',
      components: () => ([
        path.resolve(__dirname, 'src/components/molecules/FloatingBtn', 'FloatingBtn.jsx'),
        path.resolve(__dirname, 'src/components/molecules/ListItem', 'ListItem.jsx'),
        path.resolve(__dirname, 'src/components/molecules/MessageForm', 'MessageForm.jsx'),
      ])
    },{
      name: 'organisms',
      components: () => ([
        path.resolve(__dirname, 'src/components/organisms/List', 'List.jsx'),
      ])
    },{
      name: 'templates',
      components: () => ([
        path.resolve(__dirname, 'src/components/templates/ChatView', 'ChatView.jsx'),
      ])
    },{
      name: 'pages',
      components: () => ([
        
      ])
    }
  ]
}