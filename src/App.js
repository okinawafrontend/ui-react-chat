import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";

const Home = () => <h1>Hello World!!</h1>;

const Chat = () => <div>Chat Page!</div>;

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Router>
        <div>
          <Switch>
            <Route {...this.state} exact path="/" component={Home} />
            <Route {...this.state} exact path="/chat" component={Chat} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
