var app = require("express")();
var socket = require("socket.io");

var server = app.listen(8080, () => {
  console.log("Running on port 8080.");
});

var io = socket(server);

// クライアント接続時の処理
io.on("connection", socket => {
  console.log("client connected!!");

    //受け取り
    socket.on("shoot", msg => {

        console.log("shoot: " + msg);

        //ブロードキャスト送信
        io.emit("shoot", msg);

    });
});
